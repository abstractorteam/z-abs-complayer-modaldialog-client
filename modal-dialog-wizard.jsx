
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogWizard extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      folder: null,
      name: ''
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show(folder) {
    this.updateState({
      show: {$set: true},
      folder: {$set: folder},
      name: {$set: ''}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      folder: {$set: null}
    });
  }  
  
  renderErrorMessage() {
   /* if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }
  
  renderChildren() {
    return (
      <>
        <br />
        {this.props.children}
      </>
    );
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
    marginLeft: 64
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-2">
                  <label htmlFor="modal_dialog_wizard_name" className="control-label">Name</label>
                </div>
                <div className="col-sm-9">
                  <input type="text" className="form-control input-sm" placeholder={this.props.namePlaceHolder} value={this.state.name} id="modal_dialog_wizard_name"
                    onChange={(e) => {
                      this.updateState({name: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
              {this.renderChildren()}
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={0 === this.state.name.length} type="button" className="btn btn-primary"
            onClick={(e) => {
              this.props.onWizardNew(this.state.folder.projectId, `${this.state.folder.data.path}/${this.state.folder.title}`, this.state.name);
              this.hide();
            }}
          >Add</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
