
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogAbstractionAdd extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      name: '',
      description: '',
      repo: '',
      valid: false,
      error: false,
      errorText: ''
    });
  }

  didMount() {
    this.props.onLoad && this.props.onLoad();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.nameplaceholder, nextProps.nameplaceholder)
      || !this.shallowCompare(this.props.descriptionplaceholder, nextProps.descriptionplaceholder)
      || !this.shallowCompareArrayValues(this.props.repos, nextProps.repos)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show() {
    this.updateState({show: {$set: true}});
    this.updateState({name: {$set: ''}});
    this.updateState({description: {$set: ''}});
    this.updateState({repo: {$set: this.props.repos && 1 <= this.props.repos.length ? this.props.repos[0] : ''}});
    this.updateState({valid: {$set: false}});
    this.updateState({error: {$set: false}});
    this.updateState({errorText: {$set: ''}});
  }
  
  hide() {
    this.updateState({show: {$set: false}});
  }
  
  add() {
    this.props.onAdd && this.props.onAdd(this.state.name, this.state.description, this.state.repo);
  }
  
  clear() {
    this.props.onClear && this.props.onClear();
  }
  
  setName(name) {
    this.updateState({name: {$set: name}});
  }
  
  setDescription(description) {
    this.updateState({description: {$set: description}});
  }
  
  renderErrorMessage() {
    if(this.state.error) {
      const errorDivStyle = {
        float: 'left',
 	      textAlign: 'left',
        width: 440
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label" style={labelStyle}>{this.state.errorText}</label>
        </div>
      );
    }
    else if(this.props.result.code !== 'success') {
      const errorDivStyle = {
        float: 'left',
 	      textAlign: 'left',
        width: 440
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label" style={labelStyle}>{this.props.result.msg}</label>
        </div>
      );
    }
  }

  renderRepoOptions() {
    return this.props.repos.map((repo, index) => {
      return (
        <option key={index} value={repo}>{repo}</option>
      );
    });
  }
  
  renderRepo() {
    if(this.props.repos) {
      return (
        <div className="form-group">
          <label htmlFor="modal_dialog_abstraction_add_repo" className="col-sm-2 control-label">Repo</label>
          <div className="col-sm-10">
            <select className="form-control input-sm" id="modal_dialog_abstraction_add_repo" value={this.state.repo}
              onChange={(e) => {
                this.updateState({repo: {$set: e.target.value}});
              }}
            >
              {this.renderRepoOptions()}
            </select>
          </div>
        </div>
      );
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    else if(this.state.error) {
      nameDivLabelClassName += ' has-error';
    }
    const inputNameClass = this.state.error ? 'form-control has-error' : 'form-control';
    return (
      <Modal show={this.state.show || errorShow}
        onHide={(e) => {
          this.clear();
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="modal_dialog_abstraction_add_name" className="col-sm-2 control-label">Name</label>
              <div className={nameDivLabelClassName}>
                <input type="text" className="form-control" id="modal_dialog_abstraction_add_name" value={this.state.name} placeholder={this.props.nameplaceholder}
                  onChange={(e) => {
                    const name = e.target.value;
                    const first = name.charAt(0);
                    if(0 !== name.length) {
                      const valid = /^[\w,\s-]+$/gm.test(name);
                      const upperCase = first !== first.toLowerCase() && first === first.toUpperCase();
                      if(valid && upperCase) {
                        this.updateState({valid: {$set: true}});
                        this.updateState({error: {$set: false}});
                      }
                      else {
                        this.updateState({valid: {$set: false}});
                        this.updateState({error: {$set: true}});
                        if(!upperCase) {
                           this.updateState({errorText: {$set: 'The name must start with a capital letter'}});
                        }
                        else {
                          let noneValidCharacter = '';
                          for(let i = 0; i < name.length; ++i) {
                            if(!/^[\w,\s-]+$/gm.test(name.charAt(i))) {
                              noneValidCharacter = name.charAt(i);
                              break;
                            }
                          }
                          this.updateState({errorText: {$set: `The name contains not allowed characters: '${noneValidCharacter}'`}});
                        }
                      }
                    }
                    else {
                      this.updateState({valid: {$set: false}});
                      this.updateState({error: {$set: false}});
                    }
                    this.setName(e.target.value);
                  }}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="modal_dialog_abstraction_add_description" className="col-sm-2 control-label">Description</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="modal_dialog_abstraction_add_description" value={this.state.description} placeholder={this.props.descriptionplaceholder}
                  onChange={(e) => {
                    this.setDescription(e.target.value);
                  }}
                />
              </div>
            </div>
            {this.renderRepo()}
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={!this.state.valid} type="button" className="btn btn-primary"
            onClick={(e) => {
              this.add();
              this.hide();
            }}
          >Add</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.clear();
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
