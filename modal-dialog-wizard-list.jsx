
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogWizardList extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      listName: ''
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state, nextState);
  }
  
  renderlistNameOptions() {
    if(null !==this.props.listNames) {
      return this.props.listNames.map((listName, index) => {
        return (
          <option key={index} value={listName}>{listName}</option>
        );
      });
    }
  }
  
  render() {
    return (
      <div className="row" style={{textAlign: 'right'}}>
        <div className="col-sm-2">
        <label htmlFor={`modal_dialog_wizard_list_${this.props.listName}`} className="control-label">{this.props.listName}</label>
        </div>
        <div className="col-sm-9">
          <select className="form-control input-sm" id={`modal_dialog_wizard_list_${this.props.listName}`} value={this.state.listName}
            onChange={(e) => {
              this.updateState({listName: {$set: e.target.value}});
              this.props.onChosenList && this.props.onChosenList(e.target.value);
            }}
          >
            {this.renderlistNameOptions()}
          </select>
        </div>        
      </div>
    );
  }
}
