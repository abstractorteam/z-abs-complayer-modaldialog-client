
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileProperties extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      file: null,
      fileName: '',
      origFileName: ''
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }

  
  show(file) {
    this.updateState({
      show: {$set: true},
      file: {$set: file},
      fileName: {$set: file.title},
      origFileName: {$set: file.title}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      file: {$set: null}
    });
  }
  
  renderErrorMessage() {
    /*if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }

  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-2">
                  <label htmlFor="modal_dialog_file_file_properties_name" className="control-label">Name</label>
                </div>
                <div className="col-sm-9">
                  <input type="text" className="form-control input-sm" placeholder="file name" value={this.state.fileName} id="modal_dialog_file_file_properties_name"
                    onChange={(e) => {
                      this.updateState({fileName: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={(0 === this.state.fileName.length || this.state.origFileName === this.state.fileName)} type="button" className="btn btn-primary"
            onClick={(e) => {
              this.props.onFileProperties(this.state.file.projectId, this.state.file.path, this.state.file.title, this.state.fileName);
              this.hide();
            }}
          >Change</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
