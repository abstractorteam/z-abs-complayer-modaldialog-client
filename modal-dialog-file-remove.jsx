
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileRemove extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      file: null,
      doRemove: false,
      doDelete: false
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }

    
  show(file, doRemove, doDelete) {
    this.updateState({
      show: {$set: true},
      file: {$set: file},
      doRemove: {$set: doRemove},
      doDelete: {$set: doDelete}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      file: {$set: null},
      doRemove: {$set: false},
      doDelete: {$set: false}
    });
  }
  
  renderErrorMessage() {
    /*if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }

  renderText() {
    if(null !== this.state.file) {
      return `Do you really want to delete or remove: '${this.state.file.path}/${this.state.file.title}'?`;
    }
    else {
      return '';
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          {this.renderText()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={!this.state.doRemove} type="button" className="btn btn-warning"
            onClick={(e) => {
              this.props.onFileRemove(this.state.file.projectId, this.state.file.key);
              this.hide();
            }}
          >Remove</button>
          <button disabled={!this.state.doDelete} type="button" className="btn btn-danger"
            onClick={(e) => {
              this.props.onFileDelete(this.state.file.projectId, this.state.file.key);
              this.hide();
            }}
          >Delete</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
