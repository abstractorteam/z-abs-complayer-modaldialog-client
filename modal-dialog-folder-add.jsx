
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import Tree from 'z-abs-complayer-tree-client/tree';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFolderAdd extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      name: '',
      folder: null,
      types: []
    });
    this.folderIcons = new Map();
    this.folderIcons.set('actorjs', {open: 'images/icon/actor.ico', closed: 'images/icon/actor.ico'});
    this.folderIcons.set('css', {open: 'images/icon/folder_css_open.ico', closed: 'images/icon/folder_css_closed.ico'});
    this.folderIcons.set('html', {open: 'images/icon/folder_html_open.ico', closed: 'images/icon/folder_html_closed.ico'});
    this.folderIcons.set('js', {open: 'images/icon/folder_js_open.ico', closed: 'images/icon/folder_js_closed.ico'});
    this.folderIcons.set('json', {open: 'images/icon/folder_json_open.ico', closed: 'images/icon/folder_json_closed.ico'});
    this.folderIcons.set('jsx', {open: 'images/icon/folder_jsx_open.ico', closed: 'images/icon/folder_jsx_closed.ico'});
    this.folderIcons.set('nodejs', {open: 'images/icon/folder_nodejs_open.ico', closed: 'images/icon/folder_nodejs_closed.ico'});
    this.folderIcons.set('react', {open: 'images/icon/folder_react_open.ico', closed: 'images/icon/folder_react_closed.ico'});
    this.fileIcons = new Map();
    this.fileIcons.set('css', {valid: 'images/icon/css.ico', invalid: 'images/icon/css.ico'});
    this.fileIcons.set('js', {valid: 'images/icon/js.ico', invalid: 'images/icon/js_invalid.ico'});
    this.fileIcons.set('json', {valid: 'images/icon/json.ico', invalid: 'images/icon/json.ico'});
    this.fileIcons.set('jsx', {valid: 'images/icon/jsx.ico', invalid: 'images/icon/jsx.ico'});
    this.fileIcons.set('html', {valid: 'images/icon/html.ico', invalid: 'images/icon/html.ico'});
    this.fileIcons.set('md', {valid: 'images/icon/html.ico', invalid: 'images/icon/html.ico'});
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.project, nextProps.project)
      || !this.shallowCompare(this.props.current, nextProps.current)
      || !this.shallowCompare(this.props.original, nextProps.original)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show(folder) {
    let types = folder.data.types.map((type) => {
      return {
        chosen: 'actorjs' !== type,
        name: type
      }
    });
    this.updateState({
      show: {$set: true},
      name: {$set: ''},
      folder: {$set: folder},
      types: {$set: types}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false}
    });
  }
  
  clear() {
    this.props.onClear();
  }
  
  setName(name) {
    this.updateState({
      name: {$set: name}
    });    
  }
  
  setDescription(description) {
    this.updateState({
      description: description
    });    
  }
  
  renderErrorMessage() {
    if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      };
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }
  }

  renderTree() {
    let activationFilter = null !== this.props.original.folder ? `${this.props.original.folder.data.path}/` : undefined;
    return (
      <Tree id="actor_fancy_tree_modul_dialog_folder" project={this.props.project} current={this.props.current} expandCurrentFolder={true} allowExpand={false} activationFilter={activationFilter} folderIcons={this.folderIcons} fileIcons={this.fileIcons}
        onClickFile={(key, title, path, type) => {
        }}
        onClickFolder={(key, title, path, types) => {
          this.props.onChoose(`${path}/${title}`);
 
        }}
        onToggleFolder={(key, expanded) => {}}
      />
    );
  }
  
  renderTypes() {
    if(null !== this.state.folder) {
      return this.state.folder.data.types.map((type, index) => {
        if('actorjs' !== type) {
          return (
            <div key={index} className="row">
              <div className="col-sm-1" style={{textAlign: 'right'}}>
                <label htmlFor="folderNameId" className="control-label">{type}</label>
              </div>
              <div className="col-sm-1">
                <input type="checkbox" aria-label="..." autoComplete="off" checked={this.state.types[index].chosen} style={{marginTop: '4px'}}
                  onChange={(e) => {
                    this.state.types[index].chosen = e.currentTarget.checked;
                    this.updateState({types: {$set: this.state.types}});
                  }}
                />
              </div>
            </div>
          );
        }
      });
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    const disabled = null === this.props.current.folder || !this.props.current.folder.data.path.startsWith(`${this.props.original.folder.data.path}/${this.props.original.folder.title}`);
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.clear();
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody className="modal-dialog-file-body">
          {this.renderTree()}
          <hr />
          {this.renderTypes()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={disabled} type="button" className="btn btn-primary"
            onClick={(e) => {
              let types = [];
              this.state.types.forEach((type) => {
                if(type.chosen) {
                  types.push(type.name);
                }
              });
              this.props.onAdd(this.state.folder.projectId, this.props.current.folder.title, this.props.current.folder.data.path, types);
              this.hide();
            }}
          >Add</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.clear();
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
	  </Modal>
    );
  }
}
