
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogWorkspaceNew extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      appName: '',
      workspaceName: ''
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show() {
    this.updateState({
      show: {$set: true},
      appName: {$set: ''},
      workspaceName: {$set: ''}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      appName: {$set: ''},
      workspaceName: {$set: ''}
    });
  }
  
  renderErrorMessage() {
   /* if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }
  
  render() {
    let iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    let textStyle = {
    marginLeft: 64
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-3">
                  <label htmlFor="appNameId" className="control-label">App Name</label>
                </div>
                <div className="col-sm-8">
                  <input type="text" className="form-control input-sm" placeholder="app name" value={this.state.appName} id="appNameId"
                    onChange={(e) => {
                      this.updateState({appName: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
              <br />
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-3">
                  <label htmlFor="workspaceNameId" className="control-label">Workspace Name</label>
                </div>
                <div className="col-sm-8">
                  <input type="text" className="form-control input-sm" placeholder="workspace name" value={this.state.workspaceName} id="workspaceNameId"
                    onChange={(e) => {
                      this.updateState({workspaceName: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={0 === this.state.workspaceName.length} type="button" className="btn btn-primary"
            onClick={(e) => {
              this.props.onWorkspaceNew(this.state.appName, this.state.workspaceName);
              this.hide();
            }}
          >Add</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
