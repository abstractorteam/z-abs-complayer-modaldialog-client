
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileNew extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      folder: null,
      fileName: '',
      fileType: '',
      templateName: ''
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.templateNames, nextProps.templateNames)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show(folder) {
    let fileType = '';
    for(let i = 0; i < folder.data.types.length; ++i) {
      if('actorjs' !== folder.data.types[i]) {
        fileType = folder.data.types[i];
        break;
      }
    }
    const templateName = (this.props.templateNames && this.props.templateNames.length >= 1) ? this.props.templateNames[0] : '';
    this.updateState({
      show: {$set: true},
      folder: {$set: folder},
      fileName: {$set: ''},
      fileType: {$set: fileType},
      templateName: {$set: templateName}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      folder: {$set: null}
    });
  }  
  
  renderErrorMessage() {
   /* if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }
  
  rendertemplateNameOptions() {
    if(null !==this.props.templateNames) {
      return this.props.templateNames.map((templateName, index) => {
        return (
          <option key={index} value={templateName}>{templateName}</option>
        );
      });
    }
  }
  
  renderTemplateNames() {
    return (
      <div className="row" style={{textAlign: 'right'}}>
        <div className="col-sm-2">
          <label htmlFor="modal_dialog_file_new_template_name" className="control-label">Template</label>
        </div>
        <div className="col-sm-9">
          <select className="form-control input-sm" id="modal_dialog_file_new_template_name" value={this.state.templateName}
            onChange={(e) => {
              this.updateState({templateName: {$set: e.target.value}});
            }}
          >
            {this.rendertemplateNameOptions()}
          </select>
        </div>        
      </div>
    );
  }

  renderFileTypeOptions() {
    if(null !== this.state.folder) {
      return this.state.folder.data.types.map((type, index) => {
        return (
          <option key={index} value={type}>{type}</option>
        );
      });
    }
  }
  
  render() {
    let iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    let textStyle = {
    marginLeft: 64
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-2">
                  <label htmlFor="modal_dialog_file_new_name" className="control-label">Name</label>
                </div>
                <div className="col-sm-9">
                  <input type="text" className="form-control input-sm" placeholder="file name" value={this.state.fileName} id="modal_dialog_file_new_name"
                    onChange={(e) => {
                      this.updateState({fileName: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
              <br />
              {this.renderTemplateNames()}
              <br />
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-2">
                  <label htmlFor="modal_dialog_file_new_file_type" className="control-label">Type</label>
                </div>
                <div className="col-sm-9">
                  <select className="form-control input-sm" id="modal_dialog_file_new_file_type" value={this.state.fileType}
                    onChange={(e) => {
                      this.updateState({fileType: {$set: e.target.value}});
                    }}
                  >
                    {this.renderFileTypeOptions()}
                  </select>
                </div>
              </div>
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button disabled={0 === this.state.fileName.length} type="button" className="btn btn-primary"
            onClick={(e) => {
              this.props.onFileNew(this.state.folder.projectId, `${this.state.folder.data.path}/${this.state.folder.title}`, this.state.fileName, this.state.fileType, this.state.templateName);
              this.hide();
            }}
          >Add</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
