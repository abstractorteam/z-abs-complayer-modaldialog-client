
'use strict';

import Modal from 'z-abs-complayer-bootstrap-client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFolderProperties extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      folder: null,
      folderName: '',
      origFolderName: '',
      types: [],
      origTypes: []
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }

  
  show(folder) {
    let types = folder.data.types.map((type) => {
      return {
        chosen: true,
        name: type
      }
    });
    this.updateState({
      show: {$set: true},
      folder: {$set: folder},
      folderName: {$set: folder.title},
      origFolderName: {$set: folder.title},
      types: {$set: types},
      origTypes: {$set: this.shallowCopy(types)}
    });
  }
  
  hide() {
    this.updateState({
      show: {$set: false},
      folder: {$set: null}
    });
  }
  
  renderErrorMessage() {
    /*if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <label className="text-danger control-label">{this.props.result.msg}</label>
        </div>
      );
    }*/
  }

  renderTypes() {
    if(null !== this.state.folder) {
      return this.state.folder.data.types.map((type, index) => {
        if('actorjs' !== type) {
          return (
            <div key={index} className="row">
              <div className="col-sm-2" style={{textAlign: 'right'}}>
                <label htmlFor="folderNameId" className="control-label">{type}</label>
              </div>
              <div className="col-sm-2">
                <input type="checkbox" aria-label="..." autoComplete="off" checked={this.state.types[index].chosen} style={{marginTop: '12px'}}
                  onChange={(e) => {
                    this.updateState({types: {0: {chosen: {$set: e.currentTarget.checked }}}});
                  }}
                />
              </div>
            </div>
          );
        }
      });
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    return (
      <Modal aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/images/svg/AbstraktorA.svg" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="row" style={{textAlign: 'right'}}>
                <div className="col-sm-2">
                  <label htmlFor="modal_dialog_folder_properties_name" className="control-label">Name</label>
                </div>
                <div className="col-sm-9">
                  <input type="text" className="form-control input-sm" placeholder="folder name" value={this.state.folderName} id="modal_dialog_folder_properties_name"
                    onChange={(e) => {
                      this.updateState({folderName: {$set: e.target.value}});
                    }}
                  />
                </div>
              </div>
              {this.renderTypes()}
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button type="button" className="btn btn-primary" disabled={(0 === this.state.folderName.length || this.state.origFolderName === this.state.folderName) && (this.state.origTypes.every((type, index) => {
              return type.chosen === this.state.types[index].chosen && type.name === this.state.types[index].name;
            }))}
            onClick={(e) => {
              let types = this.state.types.map((type) => {
                if(type.chosen) {
                  return type.name;
                }
              });
              this.props.onFolderProperties(this.state.folder.projectId, this.state.folder.data.path, this.state.folder.title, this.state.folderName, types);
              this.hide();
            }}
          >Change</button>
          <button type="button" className="btn btn-default"
            onClick={(e) => {
              this.hide();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
